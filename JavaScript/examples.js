//This is a refresher of the JS syntax

//creates variable
var variable = "potato";

//does almost the same as var, use only when it actually is going to change or is variable 
let varibleValue;

//only assigned a value once and never again, aka  a constant value
const varibleValue;

//this a function in JavaScript which is used to print any kind of variables defined before in it or to just print any message that needs to be displayed to the user
console.log(variable);

//normal js function would look like this
function myFunction(){

}

//arrow functions
//advantage: whenyou use "this" it wil always point to what you think its going to 
const myFunction = () => {
 //list of arguments inside ()
}

//if you have a function that oly returns you could write it like this
const multiply = number => number * 2;
console.log(multiply(2));
